package gracefull

import (
	"os"
	"os/signal"
	"syscall"
)

func WaitingCompletion(events map[string]func(os.Signal)) {
	gracefulStop := make(chan os.Signal)

	// Подписываемся на сигналы от системы.
	signal.Notify(gracefulStop, syscall.SIGTERM)
	signal.Notify(gracefulStop, syscall.SIGKILL)
	signal.Notify(gracefulStop, syscall.SIGINT)

	sig := <-gracefulStop

	for key, event := range events {
		if key != sig.String() {
			continue
		}

		event(sig)
	}
}
