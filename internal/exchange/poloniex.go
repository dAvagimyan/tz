package exchange

import (
	"context"
	polo "github.com/iowar/poloniex"
	"strconv"
	"time"
	"tz/model"
)

func NewPoloniexClient() (*PoloniexClient, error) {
	client, err := polo.NewWSClient()
	return &PoloniexClient{
		wsClient: client,
	}, err
}

type PoloniexClient struct {
	wsClient *polo.WSClient
}

func (c *PoloniexClient) Listen(ctx context.Context, output chan model.RecentTrade, pairs []string) error {
	for _, pair := range pairs {
		err := c.wsClient.SubscribeMarket(pair)
		if err != nil {
			return err
		}

		go func(pair string) {
			for {

				// ожидаем сигнала завершения от контекста.
				select {
				case <-ctx.Done():
					return
				default:

				}

				elem := <-c.wsClient.Subs[pair]
				switch updates := elem.(type) {
				case []polo.MarketUpdate:
					for _, m := range updates {
						if m.TypeUpdate != `NewTrade` {
							continue
						}

						trade, ok := m.Data.(polo.NewTrade)
						if !ok {
							continue
						}

						output <- model.RecentTrade{
							Id:        strconv.FormatInt(trade.TradeId, 10),
							Pair:      pair,
							Price:     trade.Rate,
							Amount:    trade.Amount,
							Side:      trade.TypeOrder,
							Timestamp: time.Now(),
						}
					}
				}
			}
		}(pair)
	}

	return nil
}
