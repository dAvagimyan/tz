package exchange

import "errors"

type Fabric struct {
}

func (f *Fabric) Build(exchange string) (Exchange, error) {
	switch exchange {
	case PoloniexExchange:
		return NewPoloniexClient()
	default:
		return nil, errors.New(ExchangeNotFoundError)
	}
}
