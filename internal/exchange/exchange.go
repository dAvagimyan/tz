package exchange

import (
	"context"
	"tz/model"
)

type Exchange interface {
	Listen(ctx context.Context, output chan model.RecentTrade, pairs []string) error
}
