package exchange

const (
	PoloniexExchange = "poloniex"
)

const (
	ExchangeNotFoundError = `Exchange not found `
)
