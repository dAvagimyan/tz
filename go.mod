module tz

go 1.13

require (
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/iowar/poloniex v1.0.0
	github.com/shopspring/decimal v1.3.1 // indirect
	github.com/spf13/viper v1.9.0 // indirect
)
