package main

import (
	"context"
	"encoding/json"
	"log"
	"os"
	"strings"
	"syscall"
	"time"
	"tz/internal/exchange"
	"tz/internal/gracefull"
	"tz/model"
)

func init() {

}

func main() {

	exchangeConf, err := loadConfig(`config.json`)
	if err != nil {
		panic(err)
	}
	ctx := context.Background()
	output := make(chan model.RecentTrade, 1000)

	// фабрика для создания клиента биржи
	ExchangeFabric := exchange.Fabric{}

	for exchangeName, pairs := range exchangeConf {
		exchangeClient, err := ExchangeFabric.Build(exchangeName)
		if err != nil {
			panic(err)
		}

		// прослушиваем ннужные ключи.
		err = exchangeClient.Listen(ctx, output, replacePairPlace(pairs))
		if err != nil {
			panic(err)
		}

		// вывод через единый канал.
		go func() {
			for {
				log.Println(<-output)
			}
		}()
	}

	// завершаем по сигналу
	gracefull.WaitingCompletion(map[string]func(os.Signal){
		syscall.SIGTERM.String(): func(signal os.Signal) {
			ctx.Deadline()
			time.Sleep(time.Millisecond * 50)
			close(output)
		},
		syscall.SIGKILL.String(): func(signal os.Signal) {
			ctx.Deadline()
			time.Sleep(time.Millisecond * 50)
			close(output)
		},
	})

}

// переворачиваем пары в правильный порядок.
func replacePairPlace(pairs []string) []string {
	newPairs := make([]string, 0, len(pairs))
	for _, pair := range pairs {
		index := strings.Index(pair, `_`)
		newPairs = append(newPairs, pair[index+1:]+`_`+pair[0:index])
	}
	return newPairs
}

// считываем конфиг.
func loadConfig(filename string) (map[string][]string, error) {
	inputCnf := make(map[string][]string)
	configFile, err := os.Open(`config.json`)
	if err != nil {
		return inputCnf, err
	}
	defer configFile.Close()
	jsonParser := json.NewDecoder(configFile)
	err = jsonParser.Decode(&inputCnf)
	return inputCnf, err
}
