package model

import "time"

type RecentTrade struct {
	Id string // ID транзакции

	Pair string // Торговая пара (из списка выше)

	Price float64 // Цена транзакции

	Amount float64 // Объём транзакции

	Side string // Как биржа засчитала эту сделку (как buy или как sell)

	Timestamp time.Time // Время транзакции
}
